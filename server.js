var express = require("express");
var app = express();
var fs = require("fs");

app.use(express.json());

app.get("/listUsers", function(req, res) {
  fs.readFile(__dirname + "/" + "users.json", "utf8", function(err, data) {
    res.json(JSON.parse(data));
    res.end(data);
  });
});

app.post("/addUser", function(req, res) {
  // First read existing users.
  fs.readFile(__dirname + "/" + "users.json", "utf8", function(err, data) {
    data = JSON.parse(data);
    var length = Object.getOwnPropertyNames(data).length + 1;

    var user = (data[length] = req.body);

    fs.writeFile(
      __dirname + "/" + "users.json",
      JSON.stringify(data),
      { enconding: "utf-8", flag: "w" },
      function() {
        res.json(user);
        res.end(data);
      }
    );
  });
});

app.put("/alterUser/:id", function(req, res) {
  // First read existing users.
  fs.readFile(__dirname + "/" + "users.json", "utf8", function(err, data) {
    data = JSON.parse(data);
    var user = data[req.params.id];

    if (typeof user === "undefined") res.status(404).send();
    else {
      data[req.params.id] = req.body;
      user = data[req.params.id];

      fs.writeFile(
        __dirname + "/" + "users.json",
        JSON.stringify(data),
        { enconding: "utf-8", flag: "w" },
        function() {
          res.json(user);
          res.end(data);
        }
      );
    }
  });
});

app.get("/:id", function(req, res) {
  // First read existing users.
  fs.readFile(__dirname + "/" + "users.json", "utf8", function(err, data) {
    data = JSON.parse(data);
    var user = data[req.params.id];

    if (typeof user === "undefined") res.status(404).send();
    else res.json(user);

    res.end();
  });
});

app.delete("/deleteUser/:id", function(req, res) {
  // First read existing users.
  fs.readFile(__dirname + "/" + "users.json", "utf8", function(err, data) {
    data = JSON.parse(data);
    var user = data[req.params.id];

    if (typeof user === "undefined") res.status(404).send();
    else {
      delete data[req.params.id];
      fs.writeFile(
        __dirname + "/" + "users.json",
        JSON.stringify(data),
        { enconding: "utf-8", flag: "w" },
        function() {
          res.end();
        }
      );

    }
  });
});

var server = app.listen(8081, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at http://%s:%s", host, port);
});
